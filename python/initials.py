import turtle

window = turtle.Screen()
window.bgcolor("white")

def draw_a():
    a = turtle.Turtle()
    a.shape("circle")
    a.color("black")
    a.speed(2)

    a.pu()
    a.setpos(-100, 0)
    a.pd()
    a.left(120)
    a.forward(100)
    a.left(120)
    a.forward(100)
    a.left(180)
    a.forward(50)
    a.seth(0)
    a.forward(50)

def draw_w():
    w = turtle.Turtle()
    w.shape("circle")
    w.color("black")
    w.speed(2)
    
    w.pu()
    w.setpos(-90, 90)
    w.pd()
    w.right(60)
    w.forward(100)
    w.left(120)
    w.forward(50)
    w.right(120)
    w.forward(50)
    w.left(120)
    w.forward(100)

def draw_b():
    b = turtle.Turtle()
    b.shape("circle")
    b.color("black")
    b.speed(2)
    
    b.pu()
    b.setpos(100, 90)
    b.pd()
    b.seth(270)
    b.forward(100)
    b.seth(0)
    b.circle(30, 180)
    b.seth(0)
    b.circle(20, 180)

def draw_fractal():
    frac = turtle.Turtle()
    frac.pu()
    frac.setpos(-100, -100)
    frac.pd()
    frac.shape("arrow")
    frac.color("blue")
    frac.speed(10)
    for i in range(1, 37):
        frac.right(10)
        frac.circle(30)

draw_a()
draw_w()
draw_b()
draw_fractal()

window.exitonclick()
