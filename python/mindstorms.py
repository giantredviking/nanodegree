import turtle

def draw_square(some_turtle):
    for i in range(1,5):
        some_turtle.forward(100)
        some_turtle.right(90)

def draw_triangle(some_triangle):
    for i in range (1,4):
        some_triangle.forward(70)
        some_triangle.right(120)

def draw_art():
    window = turtle.Screen()
    window.bgcolor("red")
    
    brad = turtle.Turtle()
    brad.shape("turtle")
    brad.color("yellow")
    brad.speed(2)
    for i in range (1,37):
        draw_square(brad)
        brad.right(10)

    angie = turtle.Turtle()
    angie.shape("arrow")
    angie.color("blue")
    angie.circle(100)

    victoria = turtle.Turtle()
    victoria.shape("arrow")
    victoria.color("white")
    victoria.speed(2)
    draw_triangle(victoria)

    window.exitonclick()

draw_art()
