import os
from string import digits

def rename_files():
    # get file names from a folder
    file_list = os.listdir(r"C:\Users\Austin\Desktop\udacity\python\prank")
    #print(file_list)
    saved_path = os.getcwd()
    print("Current working directory is: " + saved_path)
    os.chdir(r"C:\Users\Austin\Desktop\udacity\python\prank")
    remove_digits = str.maketrans('', '', digits)
    
    # for each file, rename filename
    for file_name in file_list:
        print("Old filename: " + file_name)
        print("New filename: " + file_name.translate(remove_digits))
        os.rename(file_name, file_name.translate(remove_digits))

    os.chdir(saved_path)

rename_files()
